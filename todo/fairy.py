from datetime import datetime

from .utils import WrongDate, WeatherForecast, Traffic


def CorrectDate(func):
    def _wrap(self, date):
        if date < datetime(2000, 1, 1):
            raise WrongDate()
        else:
            return func(self, date)

    return _wrap


class Fairy:
    def __init__(self):
        self.weather_forecast = WeatherForecast()
        self.traffic = Traffic()

    @CorrectDate
    def should_i_go_outside(self, date):
        if self.weather_forecast.rain(date) > 0.5:
            return False
        elif self.weather_forecast.rain(date) > 0.25:
            if self.weather_forecast.smog(date) > 0.5:
                return False
            if self.weather_forecast.mist(date) > 0.5:
                return False
        if self.weather_forecast.smog(date) > 0.25 and \
                self.weather_forecast.mist(date) > 0.25:
            return False
        return True

    @CorrectDate
    def should_i_bring_umbrella(self, date):
        return self.weather_forecast.rain(date) >= 0.1

    @CorrectDate
    def should_i_use_bus(self, date):
        traffic_factor = self.traffic(date) * 0.25
        smog_factor = self.weather_forecast.smog(date) * 0.5
        temp_factor = 0.25 if self.weather_forecast.temp(date) <= 0 else 0
        return (traffic_factor + smog_factor + temp_factor) >= 0.5
