from pytest import fixture

from .pages import TaskPage, AdminPage, Page


def test_user_can_add_task(task_page):
    task_name = "Task1"
    start_len = len(task_page.get_tasks())

    task_page.add_task(task_name)

    assert len(task_page.get_tasks()) == start_len + 1
    assert task_page.get_tasks()[-1].text == task_name


def test_user_can_mark_task_as_completed(task_page, admin_page):
    admin_page.login()
    admin_page.delete_all_tasks()
    admin_page.add_task("Task1")

    task_page.goto()
    task_page.mark_first_task_as_completed()

    assert task_page.is_fist_task_completed()


def test_user_can_remove_all_tasks(task_page, admin_page):
    admin_page.login()
    admin_page.delete_all_tasks()
    admin_page.add_task("Task1", completed=False)
    admin_page.add_task("Task2", completed=False)
    admin_page.add_task("Task3", completed=False)

    task_page.goto()
    assert len(task_page.get_tasks()) == 3
    task_page.click_delete_all_tasks()

    assert len(task_page.get_tasks()) == 0


def test_user_can_remove_all_completed_tasks(task_page, admin_page):
    admin_page.login()
    admin_page.delete_all_tasks()
    admin_page.add_task("Task1", completed=True)
    admin_page.add_task("Task2", completed=True)
    admin_page.add_task("Task3", completed=False)

    task_page.goto()
    assert len(task_page.get_tasks()) == 3
    task_page.click_delete_all_done_tasks()

    assert len(task_page.get_tasks()) == 1


@fixture(scope="session")
def page():
    page = Page()
    yield page
    page.driver.close()


@fixture(scope="session")
def task_page(page):
    return TaskPage(page.driver)


@fixture(scope="session")
def admin_page(page):
    return AdminPage(page.driver)
