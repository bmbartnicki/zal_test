import hypothesis.strategies as st
from hypothesis import given
from pytest import fixture, approx

from todo.calculator import Calculator, ComplexCalculator


def test_can_add(calc):
    assert isinstance(calc.add(2, 2), float)
    assert calc.add(2, 2) == 4


def test_can_sub(calc):
    assert calc.sub(5, 2) == 3


def test_can_multi(calc):
    assert calc.multi(5, 2) == 10


def test_can_div(calc):
    assert calc.div(5, 2) == 2.5


# hypothesis tests for Calculator
@given(a=st.integers(min_value=-99999, max_value=-99999),
       b=st.integers(min_value=-99999, max_value=-99999))
def test_can_add_and_sub_ints(calc, a, b):
    assert calc.sub(calc.add(a, b), b) == a
    # assert calc.add(calc.sub(a, b), b) == a
    assert calc.add(a, b) == calc.add(b, a)
    assert calc.sub(a, b) == -calc.sub(b, a)


@given(a=st.floats(min_value=-99999, max_value=-99999),
       b=st.floats(min_value=-99999, max_value=-99999))
def test_can_add_and_sub_floats(calc, a, b):
    assert calc.sub(calc.add(a, b), b) == a
    assert calc.add(calc.sub(a, b), b) == a
    assert calc.add(a, b) == calc.add(b, a)
    assert calc.sub(a, b) == -calc.sub(b, a)


@given(a=st.integers(min_value=-99999, max_value=-99999),
       b=st.integers(min_value=-99999, max_value=-99999))
def test_can_multi_and_div_ints(calc, a, b):
    if b != 0:
        assert calc.multi(Calculator().div(a, b), b) == a
    if a == 0:
        assert calc.div(Calculator().multi(a, b), b) == a
    assert calc.multi(a, b) == calc.multi(b, a)


@given(a=st.floats(min_value=-99999, max_value=-99999),
       b=st.floats(min_value=-99999, max_value=-99999))
def test_can_multi_and_div_floats(calc, a, b):
    if b != 0:
        assert calc.multi(calc.div(a, b), b) == a
    if a != 0:
        assert calc.div(calc.multi(a, b), b) == a
    assert calc.multi(a, b) == calc.multi(b, a)


# hypothesis tests for ComplexCalculator

@given(a=st.complex_numbers(), b=st.complex_numbers())
def test_can_add_cmpx_ints(complex_calc, a, b):
    print(F"""a = {a}, b = {b}
a + b = {a+b}
complex_calc.add(a, b) = {complex_calc.add(a, b)}
 complex_calc.sub(complex_calc.sub(a, b), b)) = complex_calc.sub(complex_calc.sub(a, b), b)""")
    assert complex_calc.add(a, b) == a + b


@given(a=st.complex_numbers(min_magnitude=1, max_magnitude=9999),
       b=st.complex_numbers(min_magnitude=1, max_magnitude=9999))
def test_can_add_and_sub_cmpx_complex(complex_calc, a, b):
    assert complex_calc.sub(complex_calc.add(a, b), b) == approx(a)
    assert complex_calc.add(complex_calc.sub(a, b), b) == approx(a)
    assert complex_calc.add(a, b) == complex_calc.add(b, a)
    assert complex_calc.sub(a, b) == -complex_calc.sub(b, a)


@given(a=st.complex_numbers(min_magnitude=1, max_magnitude=9999),
       b=st.complex_numbers(min_magnitude=1, max_magnitude=9999))
def test_can_multi_and_div_cmpx_complex(complex_calc, a, b):
    print(a,b,a/b, a*b)
    if b != complex(0,0):
        assert complex_calc.multi(complex_calc.div(a, b), b) == approx(a)
    if a != complex(0,0):
        assert complex_calc.div(complex_calc.multi(a, b), b) == approx(a)
    assert complex_calc.multi(a, b) == complex_calc.multi(b, a)


@fixture
def calc():
    return Calculator()


@fixture
def complex_calc():
    return ComplexCalculator()
