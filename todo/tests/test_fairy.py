from datetime import datetime

from mock import MagicMock
from pytest import fixture, raises

from todo.utils import WrongDate


def test_should_bring_umbrella_when_raining_eq_02(fairy, date_1):
    fairy.weather_forecast.rain.return_value = 0.2
    assert fairy.should_i_bring_umbrella(date_1) is True


def test_should_bring_umbrella_when_raining_eq_01(fairy, date_1):
    fairy.weather_forecast.rain.return_value = 0.1
    assert fairy.should_i_bring_umbrella(date_1) is True


def test_should_not_bring_umbrella_when_raining_eq_005(fairy, date_1):
    fairy.weather_forecast.rain.return_value = 0.05
    assert fairy.should_i_bring_umbrella(date_1) is False


def test_should_use_bus_when_temp_eq_zero(fairy, date_1):
    fairy.traffic.return_value = 0.25
    fairy.weather_forecast.smog.return_value = 0.5
    fairy.weather_forecast.temp.return_value = 0
    assert fairy.should_i_use_bus(date_1) is True


def test_should_use_bus_when_temp_lt_zero(fairy, date_1):
    fairy.traffic.return_value = 0.25
    fairy.weather_forecast.smog.return_value = 0.5
    fairy.weather_forecast.temp.return_value = -1
    assert fairy.should_i_use_bus(date_1) is True


def test_should_not_use_bus_when_temp_gt_zero(fairy, date_1):
    fairy.traffic.return_value = 0.25
    fairy.weather_forecast.smog.return_value = 0.5
    fairy.weather_forecast.temp.return_value = 1
    assert fairy.should_i_use_bus(date_1) is False


def test_should_not_go_outside_when_rain_qt_05(fairy, date_1):
    fairy.weather_forecast.rain.return_value = 0.6
    assert fairy.should_i_go_outside(date_1) is False


def test_should_not_go_outside_when_smog_qt_025_and_mist_qt_025(fairy, date_1):
    fairy.weather_forecast.rain.return_value = 0.1
    fairy.weather_forecast.smog.return_value = 0.26
    fairy.weather_forecast.mist.return_value = 0.26
    assert fairy.should_i_go_outside(date_1) is False


def test_should_not_go_outside_when_rain_qt_025_and_smog_qt_05(fairy, date_1):
    fairy.weather_forecast.rain.return_value = 0.4
    fairy.weather_forecast.smog.return_value = 0.51
    fairy.weather_forecast.mist.return_value = 0.1
    assert fairy.should_i_go_outside(date_1) is False


def test_should_not_go_outside_when_rain_qt_025_and_mist_qt_05(fairy, date_1):
    fairy.weather_forecast.rain.return_value = 0.4
    fairy.weather_forecast.smog.return_value = 0.1
    fairy.weather_forecast.mist.return_value = 0.51
    assert fairy.should_i_go_outside(date_1) is False


def test_should_go_outside_when_rain_eq_025_and_smog_eq_025(fairy, date_1):
    fairy.weather_forecast.rain.return_value = 0.25
    fairy.weather_forecast.smog.return_value = 0.25
    fairy.weather_forecast.mist.return_value = 0.99
    assert fairy.should_i_go_outside(date_1) is True


def test_should_go_outside_when_rain_le_025_and_smog_le_025(fairy, date_1):
    fairy.weather_forecast.rain.return_value = 0.24
    fairy.weather_forecast.smog.return_value = 0.24
    fairy.weather_forecast.mist.return_value = 0.99
    assert fairy.should_i_go_outside(date_1) is True


def test_if_date_is_before_2000_should_use_bus_raise(fairy, date_2000_01_01, date_1999_12_31):
    fairy.should_i_use_bus(date_2000_01_01)
    with raises(WrongDate):
        fairy.should_i_use_bus(date_1999_12_31)


def test_if_date_is_before_2000_should_bring_umbrella_raise(fairy, date_2000_01_01, date_1999_12_31):
    fairy.should_i_bring_umbrella(date_2000_01_01)
    with raises(WrongDate):
        fairy.should_i_bring_umbrella(date_1999_12_31)


def test_if_date_is_before_2000_should_go_outside_raise(fairy, date_2000_01_01, date_1999_12_31):
    fairy.should_i_go_outside(date_2000_01_01)
    with raises(WrongDate):
        fairy.should_i_go_outside(date_1999_12_31)


@fixture
def fairy():
    from todo.fairy import Fairy
    _fairy = Fairy()
    _fairy.traffic = MagicMock()
    _fairy.traffic.return_value = 0.5
    _fairy.weather_forecast.rain = MagicMock()
    _fairy.weather_forecast.rain.return_value = 0.5
    _fairy.weather_forecast.mist = MagicMock()
    _fairy.weather_forecast.mist.return_value = 0.5
    _fairy.weather_forecast.temp = MagicMock()
    _fairy.weather_forecast.temp.return_value = 2
    _fairy.weather_forecast.smog = MagicMock()
    _fairy.weather_forecast.smog.return_value = 0.5
    return _fairy


@fixture
def date_1():
    return datetime(2018, 11, 11)


@fixture
def date_2():
    return datetime(2018, 11, 12)


@fixture
def date_2000_01_01():
    return datetime(2000, 1, 1)


@fixture
def date_1999_12_31():
    return datetime(1999, 12, 31)
