from contracts import contract


class Calculator:

    def add(self, a, b):
        a, b = self._to_numbers(a, b)
        return self._add(a, b)

    @contract
    def _add(self, a: float, b: float) -> float:
        return a + b

    def sub(self, a, b):
        a, b = self._to_numbers(a, b)
        return self._sub(a, b)

    @contract
    def _sub(self, a: float, b: float) -> float:
        return a - b

    def multi(self, a, b):
        a, b = self._to_numbers(a, b)
        return self._multi(a, b)

    @contract
    def _multi(self, a: float, b: float) -> float:
        return a * b

    def div(self, a, b):
        a, b = self._to_numbers(a, b)
        return a / b

    @contract
    def _div(self, a: float, b: float) -> float:
        return a / b

    def _to_numbers(self, a, b):
        return self._to_number(a), self._to_number(b)

    def _to_number(self, a):
        return float(a)


class ComplexCalculator(Calculator):
    def add(self, a, b):
        real_a, img_a, real_b, img_b = self._to_complex(a, b)
        return complex(super().add(real_a, real_b), super().add(img_a, img_b))

    def sub(self, a, b):
        real_a, img_a, real_b, img_b = self._to_complex(a, b)
        return complex(super().sub(real_a, real_b), super().sub(img_a, img_b))

    def multi(self, a, b):
        real_a, img_a, real_b, img_b = self._to_complex(a, b)
        return complex(super().multi(real_a, real_b), super().multi(img_a, img_b))

    def div(self, a, b):
        real_a, img_a, real_b, img_b = self._to_complex(a, b)
        return complex(super().div(real_a, real_b), super().div(img_a, img_b))

    def _to_complex(self, a, b):
        a = complex(a, 0) if isinstance(a, int) or isinstance(a, float) else a
        if not isinstance(a, complex):
            raise NoComplexException()
        a = complex(b, 0) if isinstance(b, int) or isinstance(b, float) else a
        if not isinstance(b, complex):
            raise NoComplexException()
        return (super()._to_number(a.real),
                super()._to_number(a.imag),
                super()._to_number(b.real),
                super()._to_number(b.imag))


class NoComplexException(Exception):
    pass
